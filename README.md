# Welcome to [Slidev](https://github.com/slidevjs/slidev)!

To start the slide show:

- `npm install`
- `npm run dev`
- visit http://localhost:3030

Edit the [slides.md](./slides.md) to see the changes.

---

# Vue Optimization

## For faster development server and optimized bundle for users

<div v-motion
  :initial="{ x: -80 }"
  :enter="{ x: 0 }" class="absolute bottom-10">
  <span class="font-700">
    06/21
  </span>
</div>


---

# Why Optimize?

<article class="grid grid-cols-2 gap-x-4 mt-4">
  <div class="text-2xl">
  For Development Server
    <ul class="text-xl">
      <li>
        Faster Compilation
      </li>
      <li>
        Faster Hot Reload
      </li>
      <li>
        Better Productivity
      </li>
    </ul>
  </div>
  <div class="text-2xl">
  For Production Server
     <ul class="text-xl">
    <li>
      Smaller Build Size
    </li>
    <li>
      Faster Page Load
    </li>
    <li>
      Happier Customer
    </li>
  </ul>
  </div>
</article>

---

<div class="text-white font-bold">
  <h1>Development Server - Before</h1>
</div>
<div class="flex">

<img class="w-5/6 mr-2" src="/image/before-optimized-dev.png">
<img v-click  class="w-50" src="/image/before-optimized-dev-dependencies.png">
</div>

---
---

<div class="text-white font-bold">
  <h1>Development Server - After</h1>
</div>
<div class="flex">

<img class="w-5/6 mr-2" src="/image/after-optimized-dev.png">

<img v-click  class="w-50" src="/image/after-optimized-dev-dependencies.png">

</div>

---

<div class="text-white font-bold">
  <h1>Dev Server Comparison</h1>
</div>

<div class="flex">

<img class="w-50 mr-2" src="/image/before-optimized-dev-dependencies.png">
<img class="w-50" src="/image/after-optimized-dev-dependencies.png">
</div>

* Reduction of 5mb
* No need to compile the whole Ant + Vuetify library 
* No need to include the whole Ant CSS library  icons
* Reduced Moment library


---
---

<div class="text-white font-bold">
  <h1>Production Server - Before</h1>
</div>
<div class="flex">

<img class="w-5/6 mr-2" src="/image/before-optimized-prod.png">

<img v-click  class="w-50" src="/image/before-optimized-prod-dependencies-gzip.png">

</div>

---
---

<div class="text-white font-bold">
  <h1>Production Server - After</h1>
</div>
<div class="flex">

<img class="w-5/6 mr-2" src="/image/after-optimized-prod.png">

<img v-click class="w-60" src="/image/after-optimized-prod-dependencies-gzip.png">

</div>

---

<div class="text-white font-bold">
  <h1>Prod Server Comparison</h1>
</div>

<div class="flex">

<img class="w-50 mr-2" src="/image/before-optimized-prod-dependencies-gzip.png">
<img class="w-50" src="/image/after-optimized-prod-dependencies-gzip.png">
</div>

* Reduction of .4mb (This is huge for production server!)
* Since we reduced Ant to 27.8kb and removed ant-design icons, we can de-prioritize removing Ant for now!
* Vuetify library and Moment is significantly smaller


---

<div class="text-white font-bold">
  <h2>Removed Mukta and Roboto assets. Use CDN</h2>
</div>
<p>Faster performance and lower latency by using servers closest to the users</p>
<div class="flex">

<img class="w-100 h-56  mr-2" src="/image/before-optimized-assets.png">
<img v-click class="w-100 h-56" src="/image/after-optimized-dev-assets.png">
</div>

<div class="grid grid-cols-2 gap-x-4 mt-4">
<div v-click>

index.html
```ts 
<link 
  href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap"
  rel="stylesheet"
/>
```

</div>

<div v-click>

src/sass/fonts.scss
```ts
  @font-face {
    src: url('https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap')
      format('truetype');
    font-family: 'Roboto';
    font-weight: 300;
    font-style: normal;
  }
```
</div>
</div>

---

# Treeshake Vuetify

## Only load the components you need

<a href="https://www.npmjs.com/package/vuetify-loader">Install vuetify loader </a>
```ts
  npm i vuetify-loader
```

vue.config.js

```ts {all|3|all}
  // extend webpack. Under the hood of Vue CLI, it uses Webpack
  chainWebpack(config) {
    config.plugin('VuetifyLoaderPlugin')
  }
```

/plugins/vuetify.ts -
https://vuetifyjs.com/en/getting-started/installation/#webpack-install
```ts
import Vuetify from "vuetify/lib";
// Remove vuetify.min.css 
// import Vuetify from "vuetify";
// import "vuetify/dist/vuetify.min.css";
```


---

# Modularize Ant-Library
<a href="https://www.antdv.com/docs/vue/getting-started/#Import-on-Demand">Import Ant modules on Demand</a>
<p>Only use Ant modules that you need, this reduces compile for dev server as well</p>
<div class="grid grid-cols-2 gap-4">
<div>
src/main.ts

```ts
// import 'ant-design-vue/dist/antd.css'
```
</div>
<div v-click>
src/components/DragAndDropUpload.vue

```ts
import { Upload } from "ant-design-vue";
import { UploadFile } from "ant-design-vue/types/upload";
// import 'ant-design-vue/lib/upload/style/css'
```
</div>
</div>
<v-clicks :every='1'>

package.json

```ts {all|2|all}
  "devDependencies": {
    "babel-plugin-import": "^1.13.3"
  }
```

.babel-rc
```ts {all|3|all}
  {
    "plugins": [
      ["import", { "libraryName": "ant-design-vue", "libraryDirectory": "es", "style": "css" }] // `style: true` for less
    ]
  }
```

</v-clicks>
---

# Remove Ant Icon

<v-clicks :every='1'>

vue.config.js

```ts {all|6-7|all}
  function resolve(dir) {
    return path.join(__dirname, dir);
  }
  ...
  chainWebpack(config) {
    config.resolve.alias
      .set('@ant-design/icons/lib/dist$', path.resolve(__dirname, './src/components/Svg/icons.js'))
  }
```

/src/Components/Svg/icons.js

```ts {3-5}
/* eslint-disable */
//This forces ant-design not to include 500kb in the build
export { default as SmileOutline } from "@ant-design/icons/lib/outline/SmileOutline";
```
</v-clicks>

---
---

# Remove Moment Locale

vue.config.js - https://github.com/jmblog/how-to-optimize-momentjs-with-webpack

```ts {all|3|all}
  chainWebpack(config) {
    config.plugin('ignore')
      .use(new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/));
  }
```

---
---

# Lazy Load Routes

src/router.ts
<!-- 
```ts {monaco}
  import TheAccountSettingsPage from '@/views/TheAccountSettingsPage.vue'
  ...
  accountsettings: {
    path: '/account-settings',
    name: 'accountsettings',
    component: TheAccountSettingsPage
  },
    // component: () => import('@/views/TheAccountSettingsPage.vue')

``` -->


<div class="grid grid-cols-2 gap-4 mt-5">

<v-click>

```ts {all|6|all}
  import TheAccountSettingsPage from '@/views/TheAccountSettingsPage.vue'
  ...
  accountsettings: {
    path: '/account-settings',
    name: 'accountsettings',
    component: TheAccountSettingsPage
  },
```

```ts {all|5|all}
  ...
  accountsettings: {
    path: '/account-settings',
    name: 'accountsettings',
    component: () => import('@/views/TheAccountSettingsPage.vue')
  },
```

</v-click>
</div>
---
layout: fact
---
# Folder Structure
Scalable Architecture
---

# Folder

use kebab-case for folders

use PascalCase for files

src/router.ts 

```ts {all|3|4|5|6}
      esg-frontend/
      └── components/
          // ├── KPI A1.2
          ├── kpi-a1.2
          // └── trial welcome
          └── trial-welcome
          └── EsgCard.vue
      └── views/
          ├── MyComponent.vue
          └── out-of-session-journey
          └── HelloWorld.ts
```


---

# Commitlint
<v-clicks :every='1'>

#### Commitlint checks if your commit messages meet the conventional commit format.

<table class="table-auto">
  <thead>
    <tr>
      <th>Type</th>
      <th>Subject</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>build</td>
      <td>Changes that affect the build system or external dependencies (example scopes: gzip, npm)</td>
    </tr>
    <tr>
      <td>ci</td>
      <td>Changes to our CI configuration files and scripts (example scopes: Travis)</td>
    </tr>
    <tr>
      <td>feat</td>
      <td>A new feature</td>
    </tr>
    <tr>
      <td>fix</td>
      <td>A bug fix</td>
    </tr>
    <tr>
      <td>perf</td>
      <td>A code change that improves performance</td>
    </tr>
    <tr>
      <td>refactor</td>
      <td>A code change that neither fixes a bug or adds a feature</td>
    </tr>
    <tr>
      <td>revert</td>
      <td>Reverts a previous commit</td>
    </tr>
    <tr>
      <td>style</td>
      <td>CSS</td>
    </tr>
    <tr>
      <td>test</td>
      <td>Adding missing tests or correcting existing tests</td>
    </tr>   
  </tbody>
</table>

</v-clicks>

<style>
  table td{
    padding: 0.70rem
  }
</style>

---

# Commitlint

After pulling from <a href="https://gitlab.com/diginexhk/esg/esg-frontend/-/tree/jeff/vue-optimization">optimized branch</a> 

Use this command to test commitlint

```ts
echo "test commit message" | npx commitlint
// --- output should be ---
⧗   input: test commit message
✖   subject may not be empty [subject-empty]
✖   type may not be empty [type-empty]

✖   found 2 problems, 0 warnings
ⓘ   Get help: https://github.com/conventional-changelog/commitlint/#what-is-commitlint
```

To apply commitlint, add a "type" at the beginning
```ts
  echo "perf: commit message" | npx commitlint
```

If there is no output then the command succeeded. 

#### To push any commits, you need to 
```ts
  git add .
  git commit -m "type: subject"
```

---

# Axios - Current

#### A lot of boilerplate to get things working

/src/components/billing
```ts
  @A(...OrganizationS.fetchOrganizationWithVoucher)
  fetchOrganizationWithVoucher: Organizations.FetchOrganizationWithVoucherA
```
/src/types/organizations.d.ts
```ts
  export type FetchOrganizationWithVoucherA = (id: string) => Promise<void>
```

<div class="grid grid-cols-2 gap-4">
<div>
  /src/types/organizationsD.ts

  ```ts
   const Decorators: {
    fetchOrganizationWithVoucher: Decorator
    // External
    extReset: string
  } = {
    fetchOrganizationWithVoucher: [Actions.fetchOrganizationWithVoucher, scope],
    // Helpers for referencing as external module
    extReset: namespace + '/' + Actions.reset
  }
  ```
</div>
<div v-click>
  /src/store/organizationStore.ts

```ts
  async [Actions.fetchOrganizationWithVoucher]({ dispatch }, id: string) {
    const url = routesApi.organizations.organizationWithVoucher(id)
    const [err, res] = await sendReq(dispatch, { url })

    if (err) return Promise.reject(err.message)

    return res
  }
```
</div>
</div>
---

# Axios - Proposal

/src/store/store.ts
```ts
Vue.prototype.$http = axios
```
/src/components/billing/AccountSubscription.vue
```ts {all|3-6|all}

   async getVoucherInformation() {
    try {
      const data = await this.$http.get(
        routesApi.organizations.organizationWithVoucher(
          this.organization.organizationName
        ).path
      )
        this.organizationWithVoucher = data
    } catch (e) {
      //console.log(e)
    }
  }
```
